const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
var pg = require('pg');
const pgp = require('pg-promise');
var db = require('./db').db;

app.listen(8080, function () {
    console.log('Listening at port 8080.');
});

app.use(express.static('.'));
app.set('views', '.');
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '.', 'index.html'));
});

var indexRoutes = require('./routes/indexRoutes');
app.use('/memory', indexRoutes);
