import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import App from '../components/App';


class AppContainer extends Component {
    constructor() {
        super();
        this.state = {

        };
    }

    async componentDidMount() {
        try {

        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <BrowserRouter>
                <App />
            </BrowserRouter>
        );
    }
}

export default AppContainer;