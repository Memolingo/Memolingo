import React, { Component } from 'react';
import '../../index.css';

class Index extends Component {
    render() {
        return (
            <div className="container">
                <header className="App-header">
                    <h1 id="App-title" className="col-md-8 offset-md-2">Memolingo</h1>
                </header>
                <br/>
                <br/>
                <br/>
                <br/>
                <div id="txtcont" className="container col-md-10 offset-md-1">
                    <p id="txt">Nauči svoje prve njemačke riječi kroz zabavnu igru <i>Memory</i> !</p>
                </div>
                <br/>
                <a id="startButton" className="col-md-4 offset-md-4" href="/memory">START</a>
            </div>
        );
    }
}

export default Index;
