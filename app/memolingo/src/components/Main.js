import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import Index from "./Index";
import Memory from './Memory';


class Main extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' render={() => <Index />} />
                <Route exact path='/memory' render={() => <Memory />} />
            </Switch>
        );
    }
}

export default Main;