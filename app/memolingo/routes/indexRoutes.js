//'use strict';
var express = require('express');
var router = express.Router();
const pgp = require('pg-promise');
var db = require('../db').db;

router.get('/', function(req, res){
    db.many('SELECT * from word ORDER BY random() LIMIT 10')
        .then(function(data) {
           console.log(data);
           res.render('memory', {data: data});
        });
});

module.exports = router;