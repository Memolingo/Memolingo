'use strict';

var promise = require('bluebird');

// pg-promise initialization options:
var options = {
    promiseLib: promise
    // // Use a custom promise library, instead of the default ES6 Promise:
    // promiseLib: promise,

    // // Extending the database protocol with our custom repositories:
    // extend: obj => {
    //     // Do not use 'require()' here, because this event occurs for every task
    //     // and transaction being executed, which should be as fast as possible.
    //     obj.users = repos.users(obj);
    //     obj.products = repos.products(obj);
    // }

};

// Load and initialize pg-promise:
var pgp = require('pg-promise')(options);
var server = require('../server');

var connectdb = 'postgres://postgres:31101981@localhost/memolingo';

// Create the database instance:
var db = pgp(connectdb);

module.exports = {

    // Library instance is often necessary to access all the useful
    // types and namespaces available within the library's root:
    pgp,

    // Database instance. Only one instance per database is needed
    // within any application.
    db
};
