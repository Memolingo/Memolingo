CREATE TABLE word (
	id SERIAL PRIMARY KEY,
	wordCro VARCHAR(255) NOT NULL UNIQUE,
	wordGer VARCHAR(255) NOT NULL UNIQUE,
	picture VARCHAR(255) NOT NULL
);

-- CREATE TABLE game (
	-- id SERIAL PRIMARY KEY
-- );

-- CREATE TABLE game_word (
	-- id SERIAL PRIMARY KEY,
	-- id_game SERIAL,
	-- id_word SERIAL
-- );

-- ALTER TABLE game_word
	-- ADD CONSTRAINT fk_id_game
    -- FOREIGN KEY (id_game) REFERENCES game(id);
	
-- ALTER TABLE game_word
	-- ADD CONSTRAINT fk_id_word
    -- FOREIGN KEY (id_word) REFERENCES word(id);
	
-- CREATE FUNCTION get_random_words() RETURNS table (idword int, wordCro varchar, wordGer varchar, picture varchar) AS $$
	-- BEGIN
		-- RETURN query 
			-- SELECT * from word
			-- ORDER BY random()
			-- LIMIT 10;
  	-- END;
-- $$ LANGUAGE plpgsql;